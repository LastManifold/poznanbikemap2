//
//  AnnotationView.swift
//  PoznanBike2
//
//  Created by Grzegorz Kurnatowski on 31.08.2018.
//  Copyright © 2018 Last Manifold. All rights reserved.
//

import UIKit
import MapKit

class AnnotationView: NSObject{
    
    let blackView = UIView()
    
    func viewAnnotation(mapView: MKMapView!, view: NewAnnotation, manager: CLLocationManager, nibView: StationView){
        
        nibView.stationName.text = String(view.title!).uppercased()
        nibView.bikes.text = view.bikes!
        nibView.bikeRacks.text = view.freeRacks! + "/" + view.bikeRacks!
        
        if (isLocationPermissionGranted() == false){
            nibView.distance.text = "Odległość: " + "Brak włączonej lokalizacji"
            
        }else{
            let bikeLoc = CLLocation(latitude: view.coordinate.latitude, longitude: view.coordinate.longitude)
            var curLocStation:CLLocation!
            curLocStation = manager.location
            let distance = curLocStation.distance(from: bikeLoc)
            nibView.distance.text = "Odległość: " + String(format:"%.0f", distance) + " m"
        }
        
        nibView.latitude = view.coordinate.latitude
        nibView.longitude = view.coordinate.longitude
    }
}

        
        
        
    
    
    
    

