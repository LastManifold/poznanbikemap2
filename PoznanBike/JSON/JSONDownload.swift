//
//  JSONDownload.swift
//  PoznanBike2
//
//  Created by Grzegorz Kurnatowski on 30.08.2018.
//  Copyright © 2018 Last Manifold. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class JSONDownload: NSObject{
        
    var getData:stationData = stationData.init(features: [])

    func JSONDownloader(mapView: MKMapView!){
        let url = URL(string: "http://www.poznan.pl/mim/plan/map_service.html?mtype=pub_transport&co=stacje_rowerowe")
        URLSession.shared.dataTask(with: url!) { (data, response, error) in
            guard let data = data else {return}
            do{
                self.getData = try JSONDecoder().decode(stationData.self, from: data)
                        for i in 0...self.getData.features.count-1{
                            let location: CLLocationCoordinate2D = CLLocationCoordinate2DMake(self.getData.features[i].geometry.coordinates[1], self.getData.features[i].geometry.coordinates[0])
                            let annotation = NewAnnotation(coordinate: location)
                            annotation.title = self.getData.features[i].properties.label
                            annotation.bikeRacks = self.getData.features[i].properties.bike_racks
                            annotation.bikes = self.getData.features[i].properties.bikes
                            annotation.freeRacks = self.getData.features[i].properties.free_racks
                            mapView?.addAnnotation(annotation)
                            }
            }
                catch let error{
                print("Error: ", error)
                    
            }
            
            }.resume()
        
    }
    
}
