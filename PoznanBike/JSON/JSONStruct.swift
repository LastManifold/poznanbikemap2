//
//  JSONStruct.swift
//  PoznanBike2
//
//  Created by Grzegorz Kurnatowski on 30.08.2018.
//  Copyright © 2018 Last Manifold. All rights reserved.
//

struct stationData: Codable{
    let features: [features]
}

struct features: Codable {
    let geometry: geometry
    let properties: properties
    let id: String
}

struct geometry: Codable {
    let coordinates: [Double]
    
}

struct properties: Codable {
    let free_racks: String
    let bikes: String
    let label: String
    let bike_racks: String
}
