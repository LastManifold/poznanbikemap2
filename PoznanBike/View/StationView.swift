//
//  StationView.swift
//  PoznanBike2
//
//  Created by Grzegorz Kurnatowski on 30.08.2018.
//  Copyright © 2018 Last Manifold. All rights reserved.
//

import UIKit
import MapKit

@IBDesignable class StationView: UIView {
    
     var latitude = 52.406464
     var longitude = 16.924997
    
    @IBOutlet weak var stationName: UILabel!
    @IBOutlet weak var bikes: UILabel!
    @IBOutlet weak var bikeRacks: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var navigateButton: UIButton!

    @IBAction func turnOnNavigation(_ sender: Any) {
       
        if (isLocationPermissionGranted() == false){
            
            let settingsAction = UIAlertAction(title: "Ustawienia", style: .default, handler: { (_) -> Void in
                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl{
                    UIApplication.shared.open(url as URL)

                }
                
            })
            
            self.navigateButton.backgroundColor = UIColor.red
            let alertTitle = "Lokalizacja niedostępna"
            let alertMessage = "Prawdopodobnie masz wyłączoną lokalizacje, lub nie zgodziłeś się na używanie lokalizacji przez tę aplikację"
            let alert = UIAlertController(title: alertTitle, message: alertMessage , preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Zamknij", style: .cancel, handler: nil))
            alert.addAction(settingsAction)
            self.window?.rootViewController?.present(alert, animated: true)
        }else{

            self.navigateButton.tintColor = UIColor.blue

        let regionDistance: CLLocationDirection = 1000
        let coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinate, regionDistance, regionDistance)
        
        let options = [MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center), MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)]
        
        let placemark = MKPlacemark(coordinate: coordinate)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = "Stacja rowerowa: " + stationName.text!
            mapItem.openInMaps(launchOptions: options)
            
        }
       
    }
    
    var stationNameText = "placeholder"
    
    var view: UIView!

    

    override init(frame: CGRect)
    {
        super.init(frame: frame)
        setup()
    }
    
    required init(coder aDecoder: NSCoder)
    {
        super.init(coder: aDecoder)!
        setup()
    }
    
    func setup()
    {

        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = UIViewAutoresizing(rawValue: UIViewAutoresizing.RawValue(UInt8(UIViewAutoresizing.flexibleWidth.rawValue) | UInt8(UIViewAutoresizing.flexibleHeight.rawValue)))
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView
    {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "StationView", bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView

        navigateButton.layer.cornerRadius = 10.0
        stationName.text = stationNameText
        
        return view
    }

}
