//
//  ViewController.swift
//  PoznanBike2
//
//  Created by Grzegorz Kurnatowski on 28.08.2018.
//  Copyright © 2018 Last Manifold. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class NewAnnotation: NSObject, MKAnnotation {
    var title: String?
    var subtitle: String?
    var coordinate: CLLocationCoordinate2D
    var bikeRacks: String?
    var bikes: String?
    var freeRacks: String?
    var buttonPushed: Bool?
    
    init(coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }
}

func isLocationPermissionGranted() -> Bool{
    guard CLLocationManager.locationServicesEnabled() else{
        return false
    }
    return [.authorizedAlways, .authorizedWhenInUse].contains(CLLocationManager.authorizationStatus())
    }


class ViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    @IBOutlet weak var mapView: MKMapView!
    
    @IBAction func myLocation(_ sender: UIButton) {
        if (isLocationPermissionGranted() == false){
            let settingsAction = UIAlertAction(title: "Ustawienia", style: .default, handler: { (_) -> Void in
                
                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl{
                    UIApplication.shared.open(url as URL)
                    }
                })
            let alertTitle = "Lokalizacja niedostępna"
            let alertMessage = "Prawdopodobnie masz wyłączoną lokalizacje, lub nie zgodziłeś się na używanie lokalizacji przez tę aplikację"
            let alert = UIAlertController(title: alertTitle, message: alertMessage, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Zamknij", style: .cancel, handler: nil))
            alert.addAction(settingsAction)
            self.present(alert, animated: true)
        }else{
            let buttonLatitude = manager.location?.coordinate.latitude
            let buttonLongtitude = manager.location?.coordinate.longitude
            let button2dCoordinate = CLLocationCoordinate2D(latitude: buttonLatitude!, longitude: buttonLongtitude!)
            let buttonSpan = MKCoordinateSpan(latitudeDelta: 0.03, longitudeDelta: 0.03)
            let buttonRegion = MKCoordinateRegion(center: button2dCoordinate, span: buttonSpan)
            
            mapView.setRegion(buttonRegion, animated: true)
        }
    }
    
    let blackView = UIView()
    let xibSubview:StationView = StationView()
    let manager = CLLocationManager()
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        self.mapView.showsUserLocation = true
    }
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.startUpdatingLocation()
        
        mapView.delegate = self
        
        let curLatitude = manager.location?.coordinate.latitude
        let curLongtitude = manager.location?.coordinate.longitude
        let curSpan = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
        var cur2dCoordinate = CLLocationCoordinate2D(latitude: 52.406464, longitude: 16.924997)
        
        if isLocationPermissionGranted(){
            cur2dCoordinate = CLLocationCoordinate2D(latitude: curLatitude!, longitude: curLongtitude!)
        }
        let curRegion = MKCoordinateRegion(center: cur2dCoordinate, span: curSpan)

        mapView.setRegion(curRegion, animated: true)
        
        let getJSON = JSONDownload()
        getJSON.JSONDownloader(mapView: mapView!)
        
    }
    
    class TranslateView: UIViewController{
        override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
            super.init(nibName: "ViewStation", bundle: nil)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }
    
    func mapView(_ MapView:MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?{
        
        if annotation is MKUserLocation{
            return nil
        }

        let pinView = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: "pin")
        pinView.markerTintColor = .blue
        pinView.glyphImage = UIImage(named: "bike_ico.png")
        
        return pinView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        let annotationView:NewAnnotation = view.annotation as! NewAnnotation
        showPin()
        let makePin = AnnotationView()
        makePin.viewAnnotation(mapView: mapView, view: annotationView, manager: manager, nibView: xibSubview)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func showPin() {
        
        if let window = UIApplication.shared.keyWindow
        {
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.5)
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(blackViewRemover)))
            window.addSubview(blackView)
            window.addSubview(xibSubview)
            let height: CGFloat = (window.frame.height)/2
            let y = window.frame.height - height
            xibSubview.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: height)
            blackView.frame = window.frame
            blackView.alpha = 0
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackView.alpha = 1
                self.xibSubview.frame = CGRect(x: 0, y: y, width: self.xibSubview.frame.width, height: self.xibSubview.frame.height)
                
            }, completion: nil)
        }
    }
    
    @objc func blackViewRemover(){
        UIView.animate(withDuration: 0.5, animations:
        {
            self.blackView.alpha = 0
            if let window = UIApplication.shared.keyWindow{
                self.xibSubview.frame = CGRect(x: 0, y: window.frame.height, width: self.xibSubview.frame.width, height: 200)
            }
        })
    }
    
}


