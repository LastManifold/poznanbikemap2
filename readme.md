# Aplikacja PoznanBike2

Aplikacja służąca do wyszukiwania stacji roweru miejskiego w Poznaniu.

## Funkcje

 - wyświetlanie wszystkich stacji na mapie
 - wyświetlanie bieżącej odległości od stacji
 - wyświetlanie ilości dostępnych rowerów 
 - możliwość uruchomienia nawigacji do wybranej stacji
 - pokazywanie aktualnej lokalizacji użytkownika

## Zrzuty ekranu

![
](https://preview.ibb.co/bNXNsz/IMG_DA330_A1995_CA_1.jpg)

Wyświetlanie stacji rowerowych

![
](https://preview.ibb.co/f4vE5K/IMG_12_CF9_DDF5_F8_C_1.jpg)

Wyświetlenie danej stacji rowerowej, wraz z przyciskiem do nawigacji. 
Przycisk uruchamia domyślną aplikacje map z gotowymi koordynatami. 

![
](https://preview.ibb.co/c7rAee/IMG_408_B13_E9087_A_1.jpg)

Wyświetlenie informacji o braku uprawnien do lokalizacji, wraz z odnośnikiem do ustawień aplikacji.
## Informacje dodatkowe

Aplikacja pobiera dane za pomocą REST API(http://www.poznan.pl/mim/plan/map_service.html?mtype=pub_transport&co=stacje_rowerowe) w formacie JSON. 
Interfejs aplikacji został wykonany przy użyniu responsywnego subview wykonanego w Interface Builderze
